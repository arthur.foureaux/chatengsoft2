create table relashionship(
    id bigint not null auto_increment,
    idUser1 bigint not null,
    idUser2 bigint not null,

    primary key(id),
    constraint fk_user1_id foreign key(idUser1) references users(id),
    constraint fk_user2_id foreign key(idUser2) references users(id)
);