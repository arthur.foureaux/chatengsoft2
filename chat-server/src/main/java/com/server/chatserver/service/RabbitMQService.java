package com.server.chatserver.service;

import com.server.chatserver.connection.RabbitMQConnection;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQService {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    RabbitMQConnection rabbitMQConnection;
    public void sendMessage(String queueName, String message){
        rabbitTemplate.convertAndSend(queueName, message);
    }
    public void addNewQueueWithDirectEschange(String nameQueue){
        rabbitMQConnection.declareQueueWithDirectExchange(nameQueue);
    }
    public void removeQueue(String nameQueue){
        rabbitMQConnection.removeQueue(nameQueue);
    }
}
