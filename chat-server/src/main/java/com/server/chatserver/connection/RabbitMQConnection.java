package com.server.chatserver.connection;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConnection {
    @Autowired
    AmqpAdmin amqpAdmin;
    DirectExchange exchange;

    public RabbitMQConnection(){
        this.exchange = new DirectExchange("amq.direct");

    }


    private Queue createQueue(String queueName){
        return new Queue(queueName, true, false, false);
    }

    private Binding bind(Queue queue, DirectExchange exchange){
        return new Binding(queue.getName(), Binding.DestinationType.QUEUE, exchange.getName(), queue.getName(), null);
    }

    public void declareQueueWithDirectExchange(String queueName){
        amqpAdmin.declareExchange(exchange);
        Queue newQueue = createQueue(queueName);

        Binding binding = bind(newQueue, exchange);

        amqpAdmin.declareQueue(newQueue);
        amqpAdmin.declareBinding(binding);
    }

    public void removeQueue(String queueName){
        amqpAdmin.deleteQueue(queueName);
    }

}
