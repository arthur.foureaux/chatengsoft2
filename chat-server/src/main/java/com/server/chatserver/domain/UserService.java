package com.server.chatserver.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Boolean isValidNickname(String nickname){
        Boolean isValid = userRepository.existsByNickname(nickname);
        return isValid;
    }
    public Boolean autenticate(UserRegistrationData userRegistrationData){
        User user = userRepository.findByNickname(userRegistrationData.nickname());

        if (user != null){
            if (user.getPassword().equals(userRegistrationData.password())){
                return true;
            }
        }

        return false;
    }
}
