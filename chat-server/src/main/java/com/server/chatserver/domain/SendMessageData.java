package com.server.chatserver.domain;

public record SendMessageData(String message, String queueName) {
}
