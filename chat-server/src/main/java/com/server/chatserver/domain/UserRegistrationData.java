package com.server.chatserver.domain;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

public record UserRegistrationData(

        @NotBlank
        String nickname,

        @NotBlank
        @Pattern(regexp = "\\d{6}")
        String password
) {
}
