package com.server.chatserver.controller;

import com.server.chatserver.domain.SendMessageData;
import com.server.chatserver.service.RabbitMQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/queue")
public class QueueController {
    @Autowired
    RabbitMQService rabbitMQService;

    @PostMapping("/send")
    public void addMessagemToQueue(@RequestBody SendMessageData sendMessageData){
        rabbitMQService.sendMessage(sendMessageData.queueName(), sendMessageData.message());
    }
    @PostMapping("/{queueName}")
    public void createNewRelationship(@PathVariable String queueName){
        System.out.print(queueName);
        rabbitMQService.addNewQueueWithDirectEschange(queueName);
    }
    @DeleteMapping
    public void removeQueue(String queueName){
        rabbitMQService.removeQueue(queueName);
    }
}
