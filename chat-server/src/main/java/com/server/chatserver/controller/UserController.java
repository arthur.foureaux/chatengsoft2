package com.server.chatserver.controller;

import com.server.chatserver.domain.UserRegistrationData;
import com.server.chatserver.domain.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class UserController {

    @Autowired
    UserService userService;
    @PostMapping
    public ResponseEntity login(@RequestBody @Valid UserRegistrationData userRegistrationData){
        if (userService.autenticate(userRegistrationData)){
            System.out.println("Usuario "+userRegistrationData.nickname()+" logado!");

            return new ResponseEntity(HttpStatus.OK);
        }
        System.out.println("Tentativa de login com falha! usuario: "+userRegistrationData.nickname());
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }


}
